// dependencies
var fs = require('fs');
var exec = require('child_process').exec;
var colors = require('colors');
var prettyFilesize = require('filesize');

// replaces .gif with .webm in a string
var generateOutputName = function(input) {
  // remove the .gif or .GIF extension
  var output = input.replace('.gif', '');
  output = output.replace('.GIF', '');

  // apply .webm and return
  return output + '.webm';
};

// calculates the compression rate and returns a percentage string like "45.34%"
var calculateCompressionRate = function(inputFileSize, outputFileSize) {
  var rate = 100 - (outputFileSize / inputFileSize * 100);
  return Math.round(rate * 100) / 100 + '%';
};

// throw an error if there are parameters missing
if (!process.argv[2]) {
  console.log('Missing at least one argument'.red);
  console.log('Usage: "node convert input.gif"');
  console.log('Advanced usage: "node convert input.gif output.webm [quality] [bitrate]"');
  console.log('                "node convert input.gif output.webm 4 1000K"');
  console.log('                [quality] goes from 4 (' + 'best'.green + ') to 12 (' + 'worst'.red + ')');
  console.log('                [bitrate] can be any reasonable bitrate. Default: 500K');
  return;
}

// set options by user or defaults
var inputName  = process.argv[2];
var outputName = process.argv[3] || generateOutputName(inputName);
var quality    = process.argv[4] || '6';
var bitrate    = process.argv[5] || '500K';

// check user input
if (parseInt(quality) < 4 || parseInt(quality) > 12) {
  console.log('Your specified quality is not supported by ffmpeg. Min: 4, Max: 12'.red);
  return;
}

var args = [
  'ffmpeg',            // call ffmpeg 
  '-i ' + inputName,   // specify input file name
  '-y',                // always overwrite output file, do not ask
  '-c:v libvpx',       // set codec
  '-crf ' + quality,   // set quality of the output. highest: 4  |  lowest: 12
  '-b:v ' + bitrate,   // set bitrate
   outputName,         // specify output file name
];

// join arguments into executable string
var command = args.join(' ');

// execute the command with 'child_process.exec'
exec(command, function (error, stdout, stderr) {
  if (error) {
    // log error in red color
    console.log(error.toString().red); 

    // delete the 0 KB file silently that ffmpeg has created
    fs.unlink(outputName, function(error) { /* silence */ }); 
    return;
  }

  // read file size and create pretty strings
  var outputFileSize = fs.statSync(outputName)['size'];
  var inputFileSize = fs.statSync(inputName)['size'];
  var compressionRateString = calculateCompressionRate(inputFileSize, outputFileSize);

  // log colored output and execute program
  console.log(outputName.green + ' (' + prettyFilesize(outputFileSize).cyan + ') has been created. Compression: ' + compressionRateString.green + ' (GIF size: ' + prettyFilesize(inputFileSize).red + ')');
});